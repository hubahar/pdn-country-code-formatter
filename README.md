PDN Country & Country Code Formatter
=========
----------------------------
Using the simple python script, you can

  - generate country code and country name combination to use in your code
  - generate country dropdown
  - Do anything :)


####Version: 1.0


Run
--------------

```
python country_parser.py
```


License
----

MIT


**Free Software, Hell Yeah!**


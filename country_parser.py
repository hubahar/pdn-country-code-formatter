from xml.dom import minidom
xmldoc = minidom.parse('countries.xml')
countryCodelist = xmldoc.getElementsByTagName('Country') 

countriesToFile = open('example.txt','w')

for country in countryCodelist :
	#Here you can put the format, the way you want to generate
	# Country Name is country.getElementsByTagName('CountryName')[0].firstChild.nodeValue
	# Country Code is country.getElementsByTagName('CountryCoded')[0].firstChild.nodeValue
	s = "The countrycode of %s is %s.\n" % (country.getElementsByTagName('CountryName')[0].firstChild.nodeValue, country.getElementsByTagName('CountryCoded')[0].firstChild.nodeValue)
	countriesToFile.write(s)
countriesToFile.close()

